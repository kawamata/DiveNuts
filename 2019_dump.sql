-- MySQL dump 10.13  Distrib 8.0.12, for osx10.13 (x86_64)
--
-- Host: localhost    Database: divenuts_development
-- ------------------------------------------------------
-- Server version	8.0.12

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8mb4 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ar_internal_metadata`
--

DROP TABLE IF EXISTS `ar_internal_metadata`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `ar_internal_metadata` (
  `key` varchar(255) NOT NULL,
  `value` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ar_internal_metadata`
--

LOCK TABLES `ar_internal_metadata` WRITE;
/*!40000 ALTER TABLE `ar_internal_metadata` DISABLE KEYS */;
INSERT INTO `ar_internal_metadata` VALUES ('environment','development','2018-09-02 03:45:06','2018-09-02 03:45:06');
/*!40000 ALTER TABLE `ar_internal_metadata` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `areas`
--

DROP TABLE IF EXISTS `areas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `areas` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `areas`
--

LOCK TABLES `areas` WRITE;
/*!40000 ALTER TABLE `areas` DISABLE KEYS */;
INSERT INTO `areas` VALUES (1,'MICRONESIA','2018-09-02 03:58:22','2018-09-02 03:58:22'),(2,'HAWAII','2018-09-02 03:58:22','2018-09-02 03:58:22'),(3,'ASIA','2018-09-02 03:58:22','2018-09-02 03:58:22'),(4,'INDIAN OCEAN','2018-09-02 03:58:22','2018-09-02 03:58:22'),(5,'SOUTH PACIFIC','2018-09-02 03:58:22','2018-09-02 03:58:22'),(6,'AUSTRALIA','2018-09-02 03:58:22','2018-09-02 03:58:22'),(7,'CARIBBEAN','2018-09-02 03:58:22','2018-09-02 03:58:22'),(8,'BAJA CALIFORNIA','2018-09-02 03:58:22','2018-09-02 03:58:22');
/*!40000 ALTER TABLE `areas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classified_sites`
--

DROP TABLE IF EXISTS `classified_sites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `classified_sites` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `divesite_id` int(11) DEFAULT NULL,
  `experience_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classified_sites`
--

LOCK TABLES `classified_sites` WRITE;
/*!40000 ALTER TABLE `classified_sites` DISABLE KEYS */;
INSERT INTO `classified_sites` VALUES (1,1,1,'2018-09-02 00:00:00','2018-09-02 00:00:00'),(2,1,2,'2018-09-02 00:00:00','2018-09-02 00:00:00'),(3,3,2,'2018-09-02 00:00:00','2018-09-02 00:00:00'),(4,2,2,'2018-09-02 00:00:00','2018-09-02 00:00:00'),(5,3,2,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(6,3,6,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(7,4,1,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(8,4,5,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(9,5,3,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(10,5,6,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(11,6,3,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(12,7,1,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(13,7,5,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(14,8,1,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(15,8,5,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(16,9,3,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(17,10,1,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(18,10,3,'2019-01-07 00:39:32','2019-01-07 00:39:32'),(19,10,5,'2019-01-07 00:39:32','2019-01-07 00:39:32');
/*!40000 ALTER TABLE `classified_sites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comments`
--

DROP TABLE IF EXISTS `comments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `comments` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `comment` text,
  `user_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `divesite_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comments`
--

LOCK TABLES `comments` WRITE;
/*!40000 ALTER TABLE `comments` DISABLE KEYS */;
INSERT INTO `comments` VALUES (1,'Yess !! This site is amazing because water is extremely clean and there were bunch fish. I will go there again next year.',4,'2019-01-07 05:42:19','2019-01-07 08:00:20',4),(2,'WOOOOOOOOOOOW! What a beautiful site!!!!!!',4,'2019-01-07 05:44:22','2019-01-07 05:44:22',4),(3,'OMG, this site is amazing because water is extremely clean and there were bunch fish. I will go there again next year.',4,'2019-01-07 05:44:54','2019-01-07 08:00:46',4),(4,'very good site! I enjoyed diving for 5 days total 10 dives and there were lots of sea creatures that I\'d never seen. There are some reliable dive shops around Pohnpei beach.',4,'2019-01-07 05:47:24','2019-01-07 05:47:24',7),(7,'WOOOOOOOOOOOW! What a beautiful site!!!!!!',4,'2019-01-07 05:59:35','2019-01-07 05:59:35',7),(9,'This is a wonderful site !!',4,'2019-01-07 06:06:50','2019-01-07 06:06:50',7),(10,'Amazing experience. I haven\'t seen such an amazing view.',4,'2019-01-07 06:08:41','2019-01-07 06:08:41',7),(11,'I will definitely dive here again. I recommend this site to all of DiveNuts.',4,'2019-01-07 06:10:22','2019-01-07 06:10:22',9),(12,'I made a lot of friends under the water.',4,'2019-01-07 06:12:51','2019-01-07 06:12:51',9),(15,'Exquisite !!',4,'2019-01-07 06:27:03','2019-01-07 06:27:03',9),(16,'This site is amazing because water is extremely clean and there were bunch fish. I will go there again next year.',4,'2019-01-07 06:27:28','2019-01-07 06:27:28',9);
/*!40000 ALTER TABLE `comments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `countries`
--

DROP TABLE IF EXISTS `countries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `countries` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `countries`
--

LOCK TABLES `countries` WRITE;
/*!40000 ALTER TABLE `countries` DISABLE KEYS */;
INSERT INTO `countries` VALUES (1,'Federated States of Micronesia','2018-09-02 04:03:30','2018-09-02 04:03:30'),(2,'Republic of Palau','2019-01-06 19:36:01','2019-01-06 19:36:01'),(3,'Hawaii','2019-01-06 19:36:01','2019-01-06 19:36:01');
/*!40000 ALTER TABLE `countries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `divesites`
--

DROP TABLE IF EXISTS `divesites`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `divesites` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `area_id` int(11) DEFAULT NULL,
  `country_id` int(11) DEFAULT NULL,
  `lat` float DEFAULT NULL,
  `long` float DEFAULT NULL,
  `level_id` varchar(255) DEFAULT NULL,
  `style_id` varchar(255) DEFAULT NULL,
  `season` varchar(255) DEFAULT NULL,
  `feature` text,
  `image_1` varchar(255) DEFAULT NULL,
  `image_2` varchar(255) DEFAULT NULL,
  `image_3` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `divesites`
--

LOCK TABLES `divesites` WRITE;
/*!40000 ALTER TABLE `divesites` DISABLE KEYS */;
INSERT INTO `divesites` VALUES (1,'Pohnpei',1,1,6.85413,158.262,'3','1','June - October','10 minutes to Blue Corner!(pic) depending on the tides. Are you kidding me? Anything that close to The Spot (a little overtouched) is a winner right out of the gates. Carp Island is an eco lodge that provides its guests with what they can grow or raise, chicken, pork, limited vegetables, papayas and of course daily fresh fish. (There aren\'t any Costcos nearby so don\'t expect too much.) If you don\'t dive there is great kayaking around the island & plenty of hammochs to nap and read all day long.','http://www.snowlandpni.net/images/4.jpg','https://allwaysdive.com.au/wp-content/uploads/2016/09/description-above-is-manta-ray-1-1024x576.jpg','https://allwaysdive.com.au/wp-content/uploads/2016/09/pk-1.jpg','2018-09-02 04:12:32','2018-09-02 04:12:32'),(3,'Blue Corner / Koror Island',3,2,7.34452,134.498,'2','1','December - June','Located at the northwest end of Ngemelis Island, the site has everything a diver could wish for. Tourists can dive this site from two different directions, depending on the current. Strong currents here attract large schools of fish and large pelagics (at a depth of 11m/36ft currents become even stronger). This dive begins on the top of a shallow coral shelf, that projects into the ocean and features vertical walls. After swimming along the lush soft coral wall for approximately 100m/300ft, divers will find a cavern with giant gorgonian sea fans.','https://www.fishnfins.com/media/k2/items/cache/ffee2447b152494b43d9816faaea83c8_XL.jpg','https://dsvsbigncb06y.cloudfront.net/site/diving/micronesia/liveaboard-micronesia-blue-corner-palau-xxl.jpg','https://i.ytimg.com/vi/Dd6j0GkRpJw/maxresdefault.jpg','2019-01-07 00:06:22','2019-01-07 00:06:22'),(4,'Chandelier Cave / Koror Island',3,2,7.34452,134.498,'2','1','December - June','This dive site is made up of a cave system with five separate caves or chambers all of which are connected and are a fantastic dive site for exploration. Found in the Malakal Harbour in Koror, the cave system at one time was an open-air cave above sea level. Over time, stalactites and stalagmites grew into formations that look like chandeliers. This created the different chambers as well. When the sea levels rose, the caves entrance was sealed and was only accessible under water. Four of the chambers are now water filled with a small chamber whilst the last one is above water but still connected to the other chambers.\r\rVisibility is excellent but divers should be careful with disturbing the sandy bottom as they enter the caves. The entrance to the caves is at around 4 metres, the bottom is quite shallow at around 12 metres, and there is no current. This dive should only be completed by qualified and trained cave divers. Divers can easily explore the first four chambers but to get to the fifth chamber, divers will have to remove their dive gear and then crawl and slide through some very tight spaces.\r\rThe caves are home to Mandarin Fish, which are only found in very few places. Cardinal fish and soldier fish are also common and along the walls of the caves, there are many crabs and shrimps.','http://fotodive.ch/wp-content/uploads/2017/07/Chandellier-Cave-Palau-FotoDive.ch-05.jpg','http://fotodive.ch/wp-content/uploads/2017/07/Chandellier-Cave-Palau-FotoDive.ch-08.jpg','http://pds.exblog.jp/pds/1/200803/13/19/c0024719_17512067.jpg','2019-01-07 00:06:22','2019-01-07 00:06:22'),(5,'Peleliu Island',3,2,7.00229,134.243,'2','2','December - June','Most of the dive centers in Palau include a trip to Peleliu offering a half day land tour along with a two tank dive. Peleliu is further from the mainland than most of the dive sites, and sometimes the trip to the Island can be rougher than others, so it is not visited as often as other locations. The extra time is well worth it, though, especially for the more experienced and adventurous diver.\r\rOn average the reef in front of the island can be found at 20 to 30 feet (7 to 10 meters). It has a steep wall that drops to 100 to 120 feet (33 to 40 meters) where it forms a plateau. The edge of the plateau drops more than a thousand feet. Against this wall is a strong converging current where the South Pacific meets the Philippine Sea. It brings strong currents, large pelagics, and nutrients that feed the coral and marine life of the wall and reef top.','https://www.sportdiver.com/sites/sportdiver.com/files/styles/1000_1x_/public/images/2018/01/spddr18_advancedadventures_0133263-jca.jpg?itok=r03Tc7MW&fc=50,50','https://www.dive-the-world.com/images/gallery/pages/medium/64ac5f621d-palau-plane-wreck.jpg','https://www.dive-the-world.com/images/gallery/pages/medium/a0f93c54bf-palau-batfish-diver.jpg','2019-01-07 00:06:22','2019-01-07 00:06:22'),(6,'Yap Islands',1,1,9.54056,138.134,'1','1','June - November','Diving in Yap is in one word, excellent! The Caroline Islands cover a vast stretch of the Western Pacific, with the remote and isolated Yap Main Islands as the most western of them. Theses islands are sparsely populated with no heavy industry, so the waters remain unpolluted.\rThe visibility is usually over 100 feet / 30 meters and at times, it is twice that. A reef surrounds the four islands with a very broad and shallow lagoon which unfortunately is too shallow for shore dives.\rThe reefs slope deeper as they move away from the shore, and they flatten out at around 30 to 60 feet and then drop off to great depths offering amazing wall dives. There are also some dive sites in the channels between the islands.\rYou can explore about 50 different dive sites in Yap\rThe northern dive sites in Yap are where the best Manta dives are. At the southern dive sites, you will find the best wall dives. All of the sites have pristine coral cover and a large variety of marine life.\r','https://palaudiveadventures.com/wp-content/uploads/2016/09/manta-ray-yap.jpg','https://www.reefrainforest.com/wp-content/uploads/2014/04/Manta-Ray-Bay-sharks2.jpg','http://scubatravel.se/cms-assets/image-galleries/yap/images/764227.yap---manta-and-diver.jpg','2019-01-07 00:06:22','2019-01-07 00:06:22'),(7,'Chuuk Islands (Truk Islands)',1,1,7.41667,151.783,'4','1','June - October','Chuuk Lagoon, or  Truk as it used to be called, is a remote atoll in the central Pacific and is a part of the Federated States of Micronesia. The islands have been inhabited since the 1300s, and according to legend, the first settlers travelled to Chuuk from Kosrae. The Germans were the first Europeans who started trading with the islands in the 1800s before the Japanese took them over after the First World War. During that period, trade flourished in the area, mainly with the export of copra.\r\rWhen World War Two broke out, everything changed, and Truk Lagoon was turned into a Japanese naval base.\r\rIn late 1943, the United States and the Allies had advanced through the Pacific Islands and were approaching Guam. On 17 February 1944, Operation Hailstone was launched by the United States. It was a massive naval air and surface attack against the Japanese fleet anchored in the lagoon. The Japanese were completely unprepared for the attack, and within two days they had lost 50 ships and over 250 planes were destroyed, many of them on the ground.','http://divemagazine.co.uk/images/article-images/travel/Chuuk_revisited/DSC00437.jpg','http://divemagazine.co.uk/images/article-images/travel/Chuuk_revisited/DSC00863.jpg','http://divemagazine.co.uk/images/article-images/travel/Chuuk_revisited/DSC01008.jpg','2019-01-07 00:06:22','2019-01-07 00:06:22'),(8,'The Sea Cave / Oahu',2,3,21.4389,-158,'3','2','All year round','The Sea Cave is an exciting cave dive located between Portlock Wall and Paliea Point. The mouth of the cave is located in 55 feet of water and the cave extends inward approximately 150 feet. You can expect to see white-tip reef sharks and sea turtles as well as many different types of shells and reef fish. After exploring the large cave divers drift along the wall with the current.','https://d1li3p7kp8c46b.cloudfront.net/7000/6242.4756.73c0a29c6f.jpg','https://www.oahuscubadiving.com/wp-content/uploads/2014/05/G0152526.jpg','https://www.oahuscubadiving.com/wp-content/uploads/2014/05/G0242552.jpg','2019-01-07 00:06:22','2019-01-07 00:06:22'),(9,'Spitting Cave  / Oahu',2,3,21.4389,-158,'3','2','All year round','This cave is made from years of consistent ocean pounding, with huge boulders that once flew out of Mt. Koko Head when she was erupting. You will notice these humongous boulders scattered around the entrance and smaller boulders inside the cave. Seeing the boulders outside the Sea Cave you can only imagine the scope of power these volcanoes once had. The entrance to the Sea Cave is about 20 feet high and 35 feet across. \rThe main entrance you should be at around 45 feet. Once inside the cave you can actually go a little deeper, the cave sinks in another 15 feet with those huge scatter boulders everywhere. The low sound of the ocean above is well heard and looking around you will will most likely see Hawaiian green sea turtles relaxing. Inside the Sea Cave continue to head to the back and soon you will notice light emitting from the surface. This is because there is a natural hole maybe 40 feet in diameter above with direct sunlight beaming down. Like they say, don\'t go towards the light, it isn\'t your time yet. This is the same case here. Innocently going up to the light or the surface of this area is very dangerous. It reminds me of a washing machine, very rough. But really, stay low don\'t go up there.','https://d1li3p7kp8c46b.cloudfront.net/7000/6241.4756.7a4eb762e7.jpg','https://marinediving.com/area/hawaii/diving/img/ph04.jpg','http://hawaii-tour.xyz/wp-content/uploads/2018/02/oafu-diving-02.jpg','2019-01-07 00:06:22','2019-01-07 00:06:22'),(10,'Manta Villege / Hawaii (Kona)',2,3,19.64,-155.997,'3','1','April - November','Mark an item off your bucket list and join us for the Manta Ray Night Dive & Snorkel EcoAdventure charter! An incredible encounter with the Kona Manta Rays at night. This world famous dive/snorkel trip has been featured on numerous Travel, National Geographic and Discovery Channel programs.  The Hawaiian Kona Coast is home to over 240 resident Manta Rays and each has been identified and named. Much like a fingerprint is unique to each human, Manta Rays have distinguishing black and white markings on their underside that are used to identify them nightly. These gentle giants can grow up to 16 feet in length and weigh up to 1600 pounds, but they have nothing that bites or stings, making this the safest large animal encounter in the world. Groups of Manta Rays converge nightly off of Makako Bay (Garden Eel Cove), to feed on the Phytoplankton & Zooplankton that appear to soak up the glow given off by underwater dive lights.  Enjoy a scenic sunset boat ride along the Kona coast to the manta location. Our friendly and knowledgeable staff will instruct you on how to encounter these amazing creatures so we can protect the mantas and their habitat. In a water depth of about 30-40 feet, both snorkelers and certified SCUBA divers are able to safely view the incredible performance by the manta rays.','https://bigislanddivers.com/wp-content/uploads/2017/02/fb-default-1.jpg','http://www.kohalatours.com/wp-content/uploads/2015/10/MantaraySnorkel.jpg','https://smhttp-ssl-64620.nexcesscdn.net//wp-content/uploads/hero-2500-fw-1-2-700x400.jpg','2019-01-07 00:06:22','2019-01-07 00:06:22');
/*!40000 ALTER TABLE `divesites` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `experiences`
--

DROP TABLE IF EXISTS `experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `experiences` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `experiences`
--

LOCK TABLES `experiences` WRITE;
/*!40000 ALTER TABLE `experiences` DISABLE KEYS */;
INSERT INTO `experiences` VALUES (1,'Adventure','2018-09-03 00:32:35','2018-09-03 00:32:35'),(2,'Intense','2018-09-03 00:32:35','2018-09-03 00:32:35'),(3,'Big Creature Manias','2018-09-03 00:32:35','2018-09-03 00:32:35'),(4,'Small Creature Friendly','2018-09-03 00:32:35','2018-09-03 00:32:35'),(5,'Extraordinary','2018-09-03 00:32:35','2018-09-03 00:32:35'),(6,'Bunch of Fish','2018-09-03 00:32:35','2018-09-03 00:32:35');
/*!40000 ALTER TABLE `experiences` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `levels`
--

DROP TABLE IF EXISTS `levels`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `levels` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `levels`
--

LOCK TABLES `levels` WRITE;
/*!40000 ALTER TABLE `levels` DISABLE KEYS */;
INSERT INTO `levels` VALUES (1,'Beginner','2018-09-02 04:09:30','2018-09-02 04:10:27'),(2,'Intermediate','2018-09-02 04:09:39','2018-09-02 04:10:44'),(3,'Advanced','2018-09-02 04:10:03','2018-09-02 04:10:03'),(4,'All Levels','2018-09-02 04:10:03','2018-09-02 04:10:03');
/*!40000 ALTER TABLE `levels` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schema_migrations`
--

DROP TABLE IF EXISTS `schema_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `schema_migrations` (
  `version` varchar(255) NOT NULL,
  PRIMARY KEY (`version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schema_migrations`
--

LOCK TABLES `schema_migrations` WRITE;
/*!40000 ALTER TABLE `schema_migrations` DISABLE KEYS */;
INSERT INTO `schema_migrations` VALUES ('20180902034328'),('20180902034359'),('20180902034412'),('20180902034934'),('20180902040245'),('20180902040646'),('20180902040654'),('20180927075734'),('20180927075957'),('20181107234303'),('20190107011126'),('20190107013126'),('20190107015253');
/*!40000 ALTER TABLE `schema_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `styles`
--

DROP TABLE IF EXISTS `styles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `styles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `styles`
--

LOCK TABLES `styles` WRITE;
/*!40000 ALTER TABLE `styles` DISABLE KEYS */;
INSERT INTO `styles` VALUES (1,'Boat','2018-09-02 04:11:10','2018-09-02 04:11:10'),(2,'Drift','2018-09-02 04:11:10','2018-09-02 04:11:10'),(3,'Beach','2018-09-02 04:11:10','2018-09-02 04:11:10');
/*!40000 ALTER TABLE `styles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL DEFAULT '',
  `encrypted_password` varchar(255) NOT NULL DEFAULT '',
  `reset_password_token` varchar(255) DEFAULT NULL,
  `reset_password_sent_at` datetime DEFAULT NULL,
  `remember_created_at` datetime DEFAULT NULL,
  `confirmation_token` varchar(255) DEFAULT NULL,
  `confirmed_at` datetime DEFAULT NULL,
  `confirmation_sent_at` datetime DEFAULT NULL,
  `unconfirmed_email` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_users_on_email` (`email`),
  UNIQUE KEY `index_users_on_reset_password_token` (`reset_password_token`),
  UNIQUE KEY `index_users_on_confirmation_token` (`confirmation_token`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (4,'kawamm0013+a@gmail.com','$2a$11$xK08tCIYx2oXzv.4t0vpD.LYJOKnH7GfSteQMXN1xrMkq/ST6xUHu',NULL,NULL,NULL,'4sYyhKNumuCva8dtycfV','2018-11-09 08:00:45','2018-11-09 07:51:47',NULL,'2018-11-09 07:51:47','2019-01-07 01:24:20','K+a');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-01-08 18:40:47
