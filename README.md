# README

* Rails version: 5.2.1

* Ruby version: 2.5.1

* Others:
	JavaScript
	jQuery
	Devise4
	Heroku

* Database: MySQL

* App description:
	This app is for scuba divers to find their next destination for diving.

* App features:
	suggesting a next diving destination / 
	sign up and sign in / 
	builing divesite API by users / 
	posting reviews / 
	Likes
