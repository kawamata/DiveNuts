$(() => {

	// トップページのアコーディオン
	const $beginners = $('#for-fun-seakers');
	const $advanced = $('#for-advanced-divers');

	$('#beginners').on('click', (e) =>{
		e.preventDefault();
		if ($advanced.css('display') == ('block')) {
			$advanced.slideToggle(500);
			setTimeout(() =>{
				$beginners.slideToggle(500);
			}, 500)
			return;
		}
		$beginners.slideToggle(500);
	});

	$('#advanced').on('click', (e) =>{
		e.preventDefault();
		if ($beginners.css('display') == ('block')) {
			$beginners.slideToggle(500);
			setTimeout(() =>{
				$advanced.slideToggle(500);
			}, 500)
			return;
		}
		$advanced.slideToggle(500);
	});


	// トップへスクロール
	const $scroll_btn = $("#js-scroll-top");
	$(window).scroll(() =>{
		if ($(this).scrollTop() > 100) {
			$scroll_btn.fadeIn();
		} else {
			$scroll_btn.fadeOut();
		}
	})
	$scroll_btn.on('click', () =>{
		$('body,html').animate({
			scrollTop: 0
		}, 500);
	});


	// ナビメニューを一定スクロールダウン後に隠す
	const $navbar = $(".navbar");
	const $navHeight = $navbar.height();
	const startPosition = 0;
	$(window).scroll(() =>{
		const $currentPossition = $(this).scrollTop();
		if ($currentPossition > startPosition) {
			if ($(window).scrollTop() > 350) {
				$navbar.css('top', '-' + $navHeight + 'px');
			}
			else {
				// 上にスクロールした場合
				$navbar.css('top', '0px');
			}
		}
	})
});

