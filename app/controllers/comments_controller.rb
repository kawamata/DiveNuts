class CommentsController < ApplicationController

	def new
		@comment = Comment.new
	end

	def create
		# binding.pry
		comment = Comment.new(comment_params)
		if comment.save
			@divesite = Divesite.find(params[:comment][:divesite_id])
			redirect_to divesite_url(id: @divesite)
		else
			redirect_back(fallback_location: root_path)
		end 
	end

	def edit
		@comment = Comment.find(params[:id])
	end

	def update
		@comment = Comment.find(params[:id])
		@comment.update(comment_params)
		redirect_to guests_show_url(id: current_user.id)
	end

	def destroy
		comment = Comment.find(params[:id])
		comment.destroy
		redirect_to guests_show_url(id: current_user.id)
	end

	private

	def comment_params
		params.require(:comment).permit(:comment, :user_id, :divesite_id)
	end
end
