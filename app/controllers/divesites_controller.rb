class DivesitesController < ApplicationController

  def index
  	area = params[:fun_dive_search][:area_id]
  	experience_1 = params[:fun_dive_search][:experience_1]
  	experience_2 = params[:fun_dive_search][:experience_2]

    # flash message
    if area.empty? && experience_1.empty? && experience_2.empty?
      redirect_to root_url, warning: "Please choose area and at least one experience."
    elsif area.empty?
      redirect_to root_url, warning: "Please choose area." 
    elsif experience_1.empty? && experience_2.empty?
      redirect_to root_url, warning: "Please choose at least one experience." 
    end

# 以下の検索を、scopeでまとめたい
    # experienceが両方とも選択されている場合
  	if !experience_1.empty? && !experience_2.empty? # empty?の逆は !empty?
    		# @divesites = Divesite.where(area_id: params[:fun_dive_search][:area_id]).includes(:experiences).where("experiences.id = #{experience_1} OR experiences.id = #{experience_2}").references(:experiences)
        @divesites = Divesite.area_id_search(area).includes(:experiences).where("experiences.id = #{experience_1} OR experiences.id = #{experience_2}").references(:experiences)

  	else
      # experienceが片方のみ選択されてる場合
  		experience = []
  		experience << experience_1 unless experience_1.empty?
  		experience << experience_2 unless experience_2.empty?
  	  	# @divesites = Divesite.where(area_id: params[:fun_dive_search][:area_id]).includes(:experiences).where("experiences.id": experience[0]).references(:experiences)	
      @divesites = Divesite.area_id_search(area).includes(:experiences).where("experiences.id": experience[0]).references(:experiences)   

    end
    
    # ----------------
  	# ↓で検索可
  	# Divesite.first.classified_site
  	# Divesite.first.experiences
  	# @divesites = Divesite.where(area_id: 1).includes(:experiences).where("experiences.id": 1).references(:experiences)
    # ----------------

  end

  def show
    @divesite = Divesite.find(params[:id])
    @comments = Comment.where(divesite: params[:id])
  end

  def like
    @divesite = Divesite.find(params[:id])
    if DivesiteLike.exists?(divesite_id: @divesite.id, user_id: current_user.id)
      DivesiteLike.find_by(divesite_id: @divesite.id, user_id: current_user.id).destroy
      redirect_back(fallback_location: root_path)
    else
      DivesiteLike.create(divesite_id: @divesite.id, user_id: current_user.id)
      redirect_back(fallback_location: root_path)
    end
  end

end
