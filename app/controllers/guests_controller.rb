class GuestsController < ApplicationController
  def show
  	@user = User.find(params[:id])
  	@comments = Comment.where(user_id: params[:id])
  end
end
