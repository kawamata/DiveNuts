class Divesite < ApplicationRecord
	has_many :classified_site
	has_many :experiences, through: :classified_site
	has_many :comments
	has_many :divesite_likes, dependent: :destroy
	belongs_to :area
	belongs_to :style
	belongs_to :level
	belongs_to :country

	# geocoded_by :address
	# after_validation :geocode

	scope :area_id_search, -> area_id { where(area_id: area_id) }
	# scope :site_search, -> { includes(:experiences).where("experiences.id = #{experience_1} OR experiences.id = #{experience_2}").references(:experiences) }

	def like_from?(user)
		self.divesite_likes.exists?(user_id: user.id)
	end
end
