class Experience < ApplicationRecord
	has_many :classified_site
	has_many :divesites, through: :classified_site
end
