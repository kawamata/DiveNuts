Rails.application.routes.draw do
	root 'home#top'

  	devise_for :users
  	resources :comments
  	
  	get 'guests/show'

  	resources :divesites, only: [:index, :show] do
  		member do
  			get :like
  		end
  	end

end
