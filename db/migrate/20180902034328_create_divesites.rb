class CreateDivesites < ActiveRecord::Migration[5.2]
  def change
    create_table :divesites do |t|
      t.string :name
      t.integer :area_id
      t.integer :country_id
      t.float :lat
      t.float :long
      t.string :level
      t.string :style
      t.string :season
      t.text :feature
      t.string :image_1
      t.string :image_2
      t.string :image_3

      t.timestamps
    end
  end
end
