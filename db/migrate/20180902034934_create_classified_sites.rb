class CreateClassifiedSites < ActiveRecord::Migration[5.2]
  def change
    create_table :classified_sites do |t|
      t.integer :divesite_id
      t.integer :experience_id

      t.timestamps
    end
  end
end
