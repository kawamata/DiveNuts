class RenameLevelToLevelIdToDivesites < ActiveRecord::Migration[5.2]
  def change
  	rename_column :divesites, :level, :level_id
  end
end
