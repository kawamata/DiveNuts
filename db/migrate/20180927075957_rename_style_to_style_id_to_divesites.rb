class RenameStyleToStyleIdToDivesites < ActiveRecord::Migration[5.2]
  def change
  	rename_column :divesites, :style, :style_id
  end
end
