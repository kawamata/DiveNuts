class CreateDivesiteLikes < ActiveRecord::Migration[5.2]
  def change
    create_table :divesite_likes do |t|
      t.integer :divesite_id
      t.integer :user_id

      t.timestamps
    end
  end
end
