
# Area.create(name:"MICRONESIA")
# Area.create(name:"HAWAII")
# Area.create(name:"ASIA")
# Area.create(name:"INDIAN OCEAN")
# Area.create(name:"SOUTH PACIFIC")
# Area.create(name:"AUSTRALIA")
# Area.create(name:"CARIBBEAN")
# Area.create(name:"BAJA CALIFORNIA")

# Divesite.create(name:"Pohnpei", area_id: 1, country_id: 1, lat: 6.854125,long: 158.262382, level: "Advance", style: "Boat", season: "June - October", feature: "10 minutes to Blue Corner!(pic) depending on the tides. Are you kidding me? Anything that close to The Spot (a little overtouched) is a winner right out of the gates. Carp Island is an eco lodge that provides its guests with what they can grow or raise, chicken, pork, limited vegetables, papayas and of course daily fresh fish. (There aren't any Costcos nearby so don't expect too much.) If you don't dive there is great kayaking around the island & plenty of hammochs to nap and read all day long.")

# Experience.create(name: "Adventure")
# Experience.create(name: "Intense")
# Experience.create(name: "Big Creature Manias")
# Experience.create(name: "Small Creature Friendly")
# Experience.create(name: "Extraordinary")

# Country.create(name: "Republic of Palau")
# Country.create(name: "Hawaii")


# require 'csv'
# CSV.foreach(Rails.root.join('db/dive_sites.csv'), encoding: 'iso-8859-1:utf-8', headers: true) do |row|
# 	Divesite.create(name: row['name'], area_id: row['area_id'], country_id: row['country_id'], lat: row['lat'], long: row['long'], level_id: row['level_id'], style_id: row['style_id'], season: row['season'], feature: row['feature'], image_1: row['image_1'], image_2: row['image_2'], image_3: row['image_3'])
# end

ClassifiedSite.create(divesite_id: 3, experience_id: 2)
ClassifiedSite.create(divesite_id: 3, experience_id: 6)
ClassifiedSite.create(divesite_id: 4, experience_id: 1)
ClassifiedSite.create(divesite_id: 4, experience_id: 5)
ClassifiedSite.create(divesite_id: 5, experience_id: 3)
ClassifiedSite.create(divesite_id: 5, experience_id: 6)
ClassifiedSite.create(divesite_id: 6, experience_id: 3)
ClassifiedSite.create(divesite_id: 7, experience_id: 1)
ClassifiedSite.create(divesite_id: 7, experience_id: 5)
ClassifiedSite.create(divesite_id: 8, experience_id: 1)
ClassifiedSite.create(divesite_id: 8, experience_id: 5)
ClassifiedSite.create(divesite_id: 9, experience_id: 3)
ClassifiedSite.create(divesite_id: 10, experience_id: 1)
ClassifiedSite.create(divesite_id: 10, experience_id: 3)
ClassifiedSite.create(divesite_id: 10, experience_id: 5)
